from datetime import date
import logging
import os
import sys

from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
import boto3


logger = logging.getLogger()  # TODO: Logger not found in Glue.
logger.setLevel(logging.INFO)
logger.info('Logger initialized.')

args = getResolvedOptions(sys.argv, ['JOB_NAME'])
sc = SparkContext()
gc = GlueContext(sc)
spark = gc.spark_session
job = Job(gc)
job.init(args['JOB_NAME'], args)
logger.info('SparkContext, GlueContext, Job initialized.')

s3 = boto3.client('s3')
logger.info('S3 client initialized.')


# Move files from input to data/
#
today = date.today()
input_bucket = 'mclennon-multicoin'
input_prefix = 'input/'
output_bucket = 'mclennon-multicoin'
output_prefix = 'data/v1/blockchain/ethereum/json/{year}/{month}/'.format(year=today.year, month=today.month)
download_dir = os.mkdir('sparktmp')

new_objects = s3.list_objects_v2(Bucket=input_bucket,
                                 Prefix=input_prefix)['Contents']
#for object_data in new_objects:
#    object_key = object_data['Key']
#    object_filename = object_key.split(input_prefix).pop()
#    logger.info('Moving object: ' + object_key)
#    s3.copy_object(Bucket=output_bucket,
#                   CopySource={'Bucket': output_bucket,
#                               'Key': object_key},
#                   Key=output_prefix+object_filename)
##    logger.info('Downloading object: ' + object_key)
##    s3.download_file(Bucket=input_bucket, Key=object_key, Filename='sparktmp/'+object_filename)

# Load JSON into Spark DataFrame
#
df = spark.read.json('s3://mclennon-multicoin/input/*.json').coalesce(1)

# Write Spark DataFrame as Parquet to warehouse/
#
df.write.save('s3://mclennon-multicoin/warehouse/ethereum/20180110/', format='parquet')

