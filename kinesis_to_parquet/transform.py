import base64
import json
import re

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq

# TODO: Firehose has some required failure handling.


def lambda_handler(event, context):
    print('event:', event)
    print('context:', context)
    payload = []
    for record in event['records']:
        payload.append(base64.b64decode(record['data']))
    make_parquet(payload)

def make_parquet(data):
    prog = re.compile('([\[:])?([-]?\d+)([,\}\]])')  # Must convert both small and large numbers for schema consistency
    for line in data:
        quoted_line = prog.sub('\\1\"\\2\"\\3', line)
        json_line = json.loads(quoted_line)
        data.append(json_line)

    df = pd.DataFrame.from_records(data)

    numeric_columns = ['blockNumber', 'gasLimit', 'gasPrice', 'gasUsed', 'index',
                   'nonce', 'timestamp', 'fromBalance', 'spotPrice',
                   'toBalance', 'value']
    df[numeric_columns] = df[numeric_columns].apply(pd.to_numeric, errors='coerce')

    table = pa.Table.from_pandas(df)
    pq.write_table(table, 's3://mclennon-multicoin/warehouse/ethlambda/example.parquet')

