import json
import re

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq


data = []
prog = re.compile('([\[:])?([-]?\d+)([,\}\]])')  # Must convert both small and large numbers for schema consistency
datafile = 'data/kinesis.txt'
with open(datafile) as f:
    for line in f:
        quoted_line = prog.sub('\\1\"\\2\"\\3', line)
        json_line = json.loads(quoted_line)
        data.append(json_line)

df = pd.DataFrame.from_records(data)

numeric_columns = ['blockNumber', 'gasLimit', 'gasPrice', 'gasUsed', 'index',
                   'nonce', 'timestamp', 'fromBalance', 'spotPrice',
                   'toBalance', 'value']
df[numeric_columns] = df[numeric_columns].apply(pd.to_numeric, errors='coerce')

table = pa.Table.from_pandas(df)
pq.write_table(table, 'data/example.parquet')

