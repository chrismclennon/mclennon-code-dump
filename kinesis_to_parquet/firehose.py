import json
import os

import boto3


client = boto3.client('firehose')
datadir = '/home/chris/Workspace/empirical_root/spark-docker/blocks'
delivery_stream_name = 'ethereum_proto'

for txnfile in os.listdir(datadir):
    print('PUT', txnfile)
    with open(os.path.join(datadir, txnfile)) as f:
        raw_json = json.load(f)
    records = [{'Data': json.dumps(transaction)+'\n'} for transaction in raw_json]
    response = client.put_record_batch(Records=records,
                                       DeliveryStreamName=delivery_stream_name)

