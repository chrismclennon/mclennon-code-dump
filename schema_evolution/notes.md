# [MUL-17] Schema Evolution

## Experimenting with Glue Crawlers

### JSON

#### Step 1: Getting started

Upload the following JSON and have Glue crawl and create a table.

```json
{"first_name": "Chris", "last_name": "McLennon", "is_married": false}
```

Crawler setting:
* How should we handle updated schemas in the data store? Update the table in the data catalog.
* How should we handle deleted objects in the data store? Mark the table deprecated in the data catalog.

This created a Glue table with the expected schema, and is queryable in Athena.

#### Step 2: Append a column

Expected result: Old row should have NULL for the new column, but no other issues.

- [ ] **QUESTION** How to update old rows to have values in the new column?

Upload the following JSON and have Glue crawl again:

```json
{"first_name": "Jenny", "last_name": "Tutone", "is_married": true, "phone_number": "512-867-5309"}
```

Actual result: Expected result happened. Old row had NULL for the new column, and the data catalog was updated appropriately.

#### Step 3: Remove a column

Expected result: One of two things. Either:  
1. The deleted column is NULL and the data catalog remains unchanged.
2. The data catalog marks the table as deprecated.

Upload the following JSON and have Glue crawl again:

```json
{"first_name": "Johnnie", "last_name": "Rico"}
```

Actual result: The first expected result. 

That makes sense. Okay, so I should use a file format with a defined schema, like csv.

#### Step 4: Delete existing object

But first, I need to move some files around. Let's see what happens if I delete an object from Step 2. Does that effectively remove phone_number from the schema?

Expected result: Mark the table as deprecated.

Delete sample_2.json and run crawler.

Actual result: Nope, it actually just removed `phone_number` from the schema.

#### Step 5: Delete all existing objects

Delete everything.

Expected result: Mark table as deprecated.

Actual result: It didn't mark the table as deprecated.

- [ ] **QUESTION** What's it take to deprecate a table using a crawler?

### CSV

#### Step 1: Now using CSVs!

Upload the following CSV. Run the crawler.

```
first_name,last_name,is_married
Chris,McLennon,false
```

It behaves as expected.

#### Step 2: Append a column

```
first_name,last_name,is_married,phone_number
Jenny,Tutone,true,512-867-5309
```

Behaves as expected.

#### Step 3: Remove a column

```
first_name,last_name
Johnnie,Rico
```

Expected result: Deprecate the table.

Actual result: The crawler did not modify the existing table. Instead it created three new tables, one for each csv uploaded. Woah.

#### Step 4: Do something weird

Add an entirely different CSV.

Expected result: Make a new table

```
zig_count,base_belong_to_us
9001,true
```

Actual result: Yup, it made a new table.

### Glue ETL

Now I'm curious how these schema changes affect Glue jobs. If I delete a column, does the Glue script need to be modified appropriately?

#### Step 1: Make a simple transform job

Upload the following csv and run the crawler to build a table:

```
first_name,last_name
Johnnie,Rico
```

Write the following ETL job:

```python
%pyspark
data = glueContext.create_dynamic_frame.from_catalog(
    database='mul17', table_name='mul17_csv')
DATA_MAPPING = [("col0", "string", "first_name", "string"), ("col1", "string", "last_name", "string")]
data_mapped = ApplyMapping.apply(frame=data, mappings=DATA_MAPPING)
```


#### Step 2: Add a column

```
first_name,last_name,is_married
Chris,McLennon,false
```

The crawler actually made two tables. I'll work around this in code.

```python
%pyspark
new_data = glueContext.create_dynamic_frame.from_catalog(
    database='mul17', table_name='mul17_sample_1_csv')
NEW_DATA_MAPPING = [("first_name", "string", "first_name", "string"), ("last_name", "string", "last_name", "string")]
new_data_mapped = ApplyMapping.apply(frame=new_data, mappings=NEW_DATA_MAPPING)
new_data_mapped.toDF().show()
```

Result:

```
+----------+---------+
|first_name|last_name|
+----------+---------+
|     Chris| McLennon|
+----------+---------+
```

The column did not get added. It obeys the ApplyMapping.