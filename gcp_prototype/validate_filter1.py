# {"index": 0, "toBalance": null, "gasLimit": 121000, "gasUsed": 21000, "currency": "ETH", "fromBalance": null, "blockTs": 1517319711, "contract": null, "blockHash": "0xc7faf8f637094c95c5f6cc131fc8aa3d8f031bcfb1df9b30b9a73f1c4532c417", "blockchain": "ethereum", "coinbase": false, "fromKey": "0xd6cb6744b7f2da784c5afd6b023d957188522198", "gasPrice": 134000000000, "toKey": "0x6f59fe38c63d94b1694250d62ccd9adf80e0ea65", "blockNumber": 5000002, "value_transferred": 2014431030000000000, "pkHash": "b6dbbcc855c41b8748376e816000132dc7e0708a28060fb3ecf6031262b044d3", "input": "0x", "spotPrice": null, "internalTx": false, "txHash": "0x5fb2dd6f2abbf409f8b3b86298ca8b243d5691bfce56d81b4ddd7a5ec646bedf", "nonce": 4080}

import json


with open('filter1') as f:
    contents = [line for line in f]

expected_columns = {'index', 'toBalance', 'gasLimit', 'gasUsed', 'currency', 'fromBalance', 'blockTs', 'contract', 'blockHash', 'blockchain', 'coinbase',
                    'fromKey', 'gasPrice', 'toKey', 'blockNumber', 'value_transferred', 'pkHash', 'input', 'spotPrice', 'internalTx', 'txHash',
                    'nonce'}
output = []
for line in contents:
    dline = json.loads(line)
    dline_keys = set(dline.keys())
    if not set(dline_keys - expected_columns):
        output.append(line)

with open('filter2', 'w') as f:
    for line in output:
        f.write(line)

