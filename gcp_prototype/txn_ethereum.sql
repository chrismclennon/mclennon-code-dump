SELECT 
  FORMAT_TIMESTAMP("%Y%m%d%H", TIMESTAMP_SECONDS(blockTs)) AS date_key
  , blockHash AS block_hash
  , blockNumber AS block_number
  , TIMESTAMP_SECONDS(blockTs) AS block_ts
  , txHash AS transaction_hash
  , index
  , contract AS contract_address
  , fromKey AS from_address
  , nonce
  , toKey AS to_address
  , value_transferred AS transaction_value
  , gasPrice * gasUsed / 1000000000000000000 AS transaction_cost
  , gasUsed AS gas_used
  , gasLimit AS gas_limit
  , gasPrice AS gas_price
  , coinbase AS is_newly_minted
  , internalTx AS is_internal_transaction
FROM 
  `elegant-device-154517.ethereum.proto_raw`
