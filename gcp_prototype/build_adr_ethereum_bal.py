from pyspark.sql import SparkSession
from pyspark.storagelevel import *

spark = SparkSession.builder.appName("mclennon").getOrCreate()

adr_ethereum = spark.read.option('header', 'true').option("inferSchema", "true").csv('gs://mclennon-multicoin/data/adr_ethereum/')

adr_ethereum.createOrReplaceTempView("adr_ethereum")
sqlQuery = """
  SELECT
    contract_address
    , address
    , DENSE_RANK() OVER (PARTITION BY contract_address ORDER BY first_block ASC) AS user_number
  FROM (
      SELECT
        contract_address
        , address
        , MIN(block_number) AS first_block
      FROM
        adr_ethereum
      WHERE
        address <> '0x0000000000000000000000000000000000000000'
      GROUP BY
        1, 2
  ) AS tbl
  """
contract_address_user_number = spark.sql(sqlQuery)

contract_address_user_number.createOrReplaceTempView("contract_address_user_number")
sqlQuery = """
  SELECT
    adr_ethereum.address
    , adr_ethereum.block_ts
    , adr_ethereum.block_number
    , adr_ethereum.transaction_hash
    , adr_ethereum.contract_address
    , contract_address_user_number.user_number
    , adr_ethereum.transaction_value
    , SUM(transaction_value) OVER (PARTITION BY adr_ethereum.address, adr_ethereum.contract_address ORDER BY adr_ethereum.block_number ASC) AS balance
  FROM
    adr_ethereum
    INNER JOIN contract_address_user_number ON
      adr_ethereum.address = contract_address_user_number.address
      AND adr_ethereum.contract_address = contract_address_user_number.contract_address
  """
adr_ethereum_balance = spark.sql(sqlQuery)

adr_ethereum_balance.write.option("header", "true").csv("gs://mclennon-multicoin/data/20180331_1/adr_ethereum_balance/", compression="gzip")

