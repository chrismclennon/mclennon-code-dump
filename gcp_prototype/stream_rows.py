from google.cloud import pubsub

publish_client = pubsub.PublisherClient()
topic = 'projects/elegant-device-154517/topics/proto_ethereum'

with open('sample_filtered_data') as f:
    for line in f:
        print(line)
        message = str.encode(line)
        publish_client.publish(topic, message)

