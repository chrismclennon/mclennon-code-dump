import json
import os
import sys

dirname = sys.argv[1]
all_filepaths = os.listdir(dirname)

for filepath in all_filepaths:
    full_infilepath = os.path.join(dirname, filepath)
    full_outfilepath = os.path.join(dirname, filepath+'_index')
    with open(full_infilepath) as infile, open(full_outfilepath, 'w') as outfile:
        for line in infile:
            line_json = json.loads(line)
            if 'internal_index' not in line_json:
                line_json['internal_index'] = None
            outfile.write(json.dumps(line_json)+'\n')

