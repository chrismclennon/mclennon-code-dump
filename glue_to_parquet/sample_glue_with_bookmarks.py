import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame

## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
## @type: DataSource
## @args: [database = "multicoin", table_name = "rawethereum", transformation_ctx = "datasource0"]
## @return: datasource0
## @inputs: []
datasource0 = glueContext.create_dynamic_frame.from_catalog(database = "multicoin", table_name = "rawethereum", transformation_ctx = "datasource0")
## @type: ApplyMapping
## @args: [mapping = [("spotprice", "string", "spotprice", "string"), ("blockhash", "string", "blockhash", "string"), ("blocknumber", "int", "blocknumber", "int"), ("timestamp", "int", "timestamp", "int"), ("from", "string", "from", "string"), ("contract", "string", "contract", "string"), ("to", "string", "to", "string"), ("nonce", "int", "nonce", "int"), ("input", "string", "input", "string"), ("coinbase", "boolean", "coinbase", "boolean"), ("hash", "string", "hash", "string"), ("tobalance", "decimal(22,0)", "tobalance", "decimal(22,0)"), ("index", "int", "index", "int"), ("gasused", "long", "gasused", "long"), ("gasprice", "long", "gasprice", "long"), ("value", "decimal(20,0)", "value", "decimal(20,0)"), ("currency", "string", "currency", "string"), ("gaslimit", "int", "gaslimit", "int"), ("frombalance", "decimal(22,0)", "frombalance", "decimal(22,0)"), ("blockchain", "string", "blockchain", "string")], transformation_ctx = "applymapping1"]
## @return: applymapping1
## @inputs: [frame = datasource0]
applymapping1 = ApplyMapping.apply(frame = datasource0, mappings = [("spotprice", "string", "spotprice", "string"), ("blockhash", "string", "blockhash", "string"), ("blocknumber", "int", "blocknumber", "int"), ("timestamp", "int", "timestamp", "int"), ("from", "string", "from", "string"), ("contract", "string", "contract", "string"), ("to", "string", "to", "string"), ("nonce", "int", "nonce", "int"), ("input", "string", "input", "string"), ("coinbase", "boolean", "coinbase", "boolean"), ("hash", "string", "hash", "string"), ("tobalance", "decimal(22,0)", "tobalance", "decimal(22,0)"), ("index", "int", "index", "int"), ("gasused", "long", "gasused", "long"), ("gasprice", "long", "gasprice", "long"), ("value", "decimal(20,0)", "value", "decimal(20,0)"), ("currency", "string", "currency", "string"), ("gaslimit", "int", "gaslimit", "int"), ("frombalance", "decimal(22,0)", "frombalance", "decimal(22,0)"), ("blockchain", "string", "blockchain", "string")], transformation_ctx = "applymapping1")
## @type: ResolveChoice
## @args: [choice = "make_struct", transformation_ctx = "resolvechoice2"]
## @return: resolvechoice2
## @inputs: [frame = applymapping1]
resolvechoice2 = ResolveChoice.apply(frame = applymapping1, choice = "make_struct", transformation_ctx = "resolvechoice2")
## @type: DropNullFields
## @args: [transformation_ctx = "dropnullfields3"]
## @return: dropnullfields3
## @inputs: [frame = resolvechoice2]
dropnullfields3 = DropNullFields.apply(frame = resolvechoice2, transformation_ctx = "dropnullfields3")

# Convert to a dataframe and partition based on "partition_col"
coalesce0 = dropnullfields3.toDF().coalesce(1)

# Convert back to a DynamicFrame for further processing.
final = DynamicFrame.fromDF(coalesce0, glueContext, "coalesce0")

## @type: DataSink
## @args: [connection_type = "s3", connection_options = {"path": "s3://mclennon-multicoin/warehouse/glue/ethereum/"}, format = "parquet", transformation_ctx = "datasink4"]
## @return: datasink4
## @inputs: [frame = dropnullfields3]
datasink4 = glueContext.write_dynamic_frame.from_options(frame = final, connection_type = "s3", connection_options = {"path": "s3://mclennon-multicoin/warehouse/glue/ethereum/"}, format = "parquet", transformation_ctx = "datasink4")
job.commit()
