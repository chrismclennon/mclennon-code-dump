import sys

from awsglue.transforms import ApplyMapping
from awsglue.dynamicframe import DynamicFrame
from awsglue.utils import getResolvedOptions
from awsglue.context import GlueContext
from awsglue.job import Job
from pyspark.context import SparkContext
import boto3


# TODO: Add logging.
# TODO: Add arguments from trigger or yaml file.

# ARGUMENTS
BUCKET = 'mclennon-multicoin'
WAREHOUSE_PREFIX = 'warehouse/v1/ethereum/'
RAW_DATABASE = 'raw-multicoin'
RAW_TABLE_NAME = 'raw_ethereum'
DATA_MAPPING = [("spotprice", "string", "spotprice", "string"), ("blockhash", "string", "blockhash", "string"), ("blocknumber", "int", "blocknumber", "int"), ("timestamp", "int", "timestamp", "int"), ("from", "string", "from", "string"), ("contract", "string", "contract", "string"), ("to", "string", "to", "string"), ("nonce", "int", "nonce", "int"), ("input", "string", "input", "string"), ("coinbase", "boolean", "coinbase", "boolean"), ("hash", "string", "hash", "string"), ("tobalance", "decimal(22,0)", "tobalance", "decimal(22,0)"), ("index", "int", "index", "int"), ("gasused", "long", "gasused", "long"), ("gasprice", "long", "gasprice", "long"), ("value", "decimal(20,0)", "value", "decimal(20,0)"), ("currency", "string", "currency", "string"), ("gaslimit", "int", "gaslimit", "int"), ("frombalance", "decimal(22,0)", "frombalance", "decimal(22,0)"), ("blockchain", "string", "blockchain", "string")]

WAREHOUSE_S3PATH = '/'.join(['s3:/', BUCKET, WAREHOUSE_PREFIX])

args = getResolvedOptions(sys.argv, ['JOB_NAME'])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

s3 = boto3.client('s3')


# TODO: Break out into a common library.
def list_all_files(bucket, prefix):
    # TODO: Add recursive listing.
    response = s3.list_objects_v2(Bucket=bucket, Prefix=prefix)
    if response.get('Contents') is None:
        return []
    all_files = [obj.get('Key') for obj in response.get('Contents')]
    while response.get('NextContinuationToken'):
        response = s3.list_objects_v2(
                        Bucket=bucket, Prefix=prefix,
                        ContinuationToken=response['NextContinuationToken'])
        next_files = [obj.get('Key') for obj in response.get('Contents')]
        all_files.extend(next_files)
    return all_files

# TODO: Break out into a common library.
def delete_files(bucket, prefixes):
    page_size = 1000
    for lower_bound in range(0, len(prefixes), page_size):
        upper_bound = min(lower_bound+page_size, len(prefixes))
        objects = [{'Key': prefix}
                   for prefix in prefixes[lower_bound:upper_bound]]
        s3.delete_objects(Bucket=bucket,
                          Delete={'Objects': objects})


existing_warehouse_files = list_all_files(bucket=BUCKET,
                                          prefix=WAREHOUSE_PREFIX)

new_data = glueContext.create_dynamic_frame.from_catalog(
        database=RAW_DATABASE, table_name=RAW_TABLE_NAME,
        transformation_ctx='new_data')
new_data_mapped = ApplyMapping.apply(
        frame=new_data, mappings=DATA_MAPPING,
        transformation_ctx='new_data_mapped')
new_data_coalesce = new_data_mapped.toDF().coalesce(1)
output = DynamicFrame.fromDF(new_data_coalesce, glueContext, 'output')

# SQUASH during the day
# Commented out this feature since the gap between deletion and rewrite was noticeable.
#
# if existing_warehouse_files:
#     old_data = glueContext.create_dynamic_frame_from_options(
#             connection_type='s3',
#             connection_options={'path': WAREHOUSE_S3PATH},
#             format='parquet').toDF()
#     all_data = old_data.unionAll(new_data_mapped)
#     all_data = all_data.coalesce(1)
#     output = DynamicFrame.fromDF(all_data, glueContext, 'output')
#     delete_files(bucket=BUCKET, prefixes=existing_warehouse_files)
# else:
#     new_data_mapped = new_data_mapped.coalesce(1)
#     output = DynamicFrame.fromDF(new_data_mapped, glueContext, 'output')

glueContext.write_dynamic_frame.from_options(
        frame = output, connection_type='s3',
        connection_options={'path': WAREHOUSE_S3PATH}, format='parquet',
        transformation_ctx='output')
job.commit()

